module.exports = function(grunt){
	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json'),
		concat:{
			 build: {
				 src: ['dev/**/*.*', '!dev/index.html'],
				dest:'deploy/main.js'
			 }
		 },
		uglify:{
			build: {
				src: 'deploy/main.js',
				dest: 'deploy/main.js'
			},
		},
		connect:{
			server: {
				options:{
					port:9001,
					base:"",
					keepalive:true
				}
			}
		},
		jshint:{
			  all: ['Gruntfile.js','dev/*.js']
		},
		copy:{
			build: {
				expand: true,
				cwd: 'dev/',
				src: ['index.html'],
				dest: 'deploy/',
				flatten: true,
				filter: 'isFile',
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-copy');
 	grunt.registerTask('build',['jshint','concat:build','uglify:build','copy:build']);
	grunt.registerTask('debug', ['jshint','concat:build','copy:build']);
};