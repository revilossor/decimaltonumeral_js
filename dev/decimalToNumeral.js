function convert(){
	var input = parseInt(document.getElementById("input").value);
	if(input===''||input<=0||isNaN(input)){document.getElementById("input").value = 'enter positive integer'; return;}
	document.getElementById('results').innerHTML += input + ' in roman numerals is ' + getNumeral(input) + '<br>';
}
var NUMERAL = 0; var DECIMAL = 1;
function getNumeral(decimal, numeral){
	numeral = (typeof numeral === 'undefined')?numeral='':numeral;
	if (decimal === 0) { return numeral; }
	var obj = getNumeralObject(decimal);
	return getNumeral(decimal - obj[DECIMAL], numeral += obj[NUMERAL]);
}
var numeralObjects = [
	['M',1000],
	['D',500],
	['C',100],
	['L',50],
	['X',10],
	['V',5],
	['I',1]
];
function getNumeralObject(decimal){
	for (var i = 0; i < numeralObjects.length; i++) {
		if (decimal >= numeralObjects[i][DECIMAL]) { return numeralObjects[i]; }
	}
	return '';
}